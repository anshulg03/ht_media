#!/usr/bin/python

__author__ = "Satish Jasthi"
import os

RESULTS_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'results')
AD_IMAGE_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'results/images')
NEWS_IMAGES = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/news_paper_images')
PDFs_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/pdfs')
SAVE_PDF_IMG_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'results/pdf_images')
LOW_RES_IMAGES = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'results/low_res_pdf_images')
TRAIN_CSV = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/train.csv')
TEST_CSV = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/test.csv')
TRAIN_IMGS = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/train')
TEST_IMGS = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/test')
TRAIN_RECORD_DATA = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/train.record')
TEST_RECORD_DATA = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'train_frcnn/data/test.record')
TF_RECORD_CSV = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'results/csv_for_tfrecords.csv')
TF_RECORD_DATA = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/new_paper_images_tfrecord.record')
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TRAIN = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/train.csv')
TEST = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/test.csv')
TRAIN_RECORD_DATA = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/train.record')
TEST_RECORD_DATA = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/test.record')
TF_RECORD_CSV = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'results/csv_for_tfrecords.csv')
TF_RECORD_DATA = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/new_paper_images_tfrecord.record')
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
