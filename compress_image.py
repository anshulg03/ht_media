#!/usr/bin/python
"""
Module to reduce size of images with appreciable compramize in
quality
"""
import glob
import os
import multiprocessing

from PIL import Image, ImageFile

from settings import SAVE_PDF_IMG_PATH, LOW_RES_IMAGES

__author__ = "Satish Jasthi"

ImageFile.LOAD_TRUNCATED_IMAGES = True


def compress_image(file_path):
    """
    Function to convert image to a low resolution image
    :param file_path: (str) Image path
    :return:
    """
    image, image_name = Image.open(file_path), file_path.split('/')[-1]
    new_file_name = os.path.join(LOW_RES_IMAGES, 'Compressed_{}'.format(image_name) )
    image.save(new_file_name, optimize=True, quality = 1)


def main():
    files = glob.glob(SAVE_PDF_IMG_PATH + '/*')
    p = multiprocessing.Pool(processes=multiprocessing.cpu_count())
    processes = [p.apply_async(compress_image, args=(each_file,)) for each_file in files]
    [each_process.get() for each_process in processes]
    p.close()


if __name__ == "__main__":
    main()


