import os,io,urllib
import google.cloud.vision
import spacy

from settings import PROJECT_ROOT
nlp = spacy.load("en_core_web_md")


def get_info(url='', file_path=''):
    """
    detects logo,text and web entities from image
    :param url:
    :param file_path:
    :return: text,logos and web entities
    """

    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(PROJECT_ROOT, "Logo_Detection/ADtext-590d9629b991.json")
    client = google.cloud.vision.ImageAnnotatorClient()

    if url:
        urllib.urlretrieve(url, "image.png")
        file_path = "image.png"

    with io.open(file_path, 'rb') as image_file:
            content = image_file.read()

    image = google.cloud.vision.types.Image(content=content)
    
    logos = client.logo_detection(image=image).logo_annotations
    entities = client.web_detection(image=image).web_detection.web_entities
    texts = client.text_detection(image=image).text_annotations

    text = [text.description for text in texts][0]
    logo = [logo.description for logo in logos]
    web_entities = [entity.description for entity in entities]

    return text, logo, web_entities


def check_for_ner(line):
    """
    checks if any "org" ner is present in the given line
    :param line:
    :return: org
    """
    org = []
    doc = nlp(unicode(line))
    for entity, tokens in zip(doc.ents, doc):
        if entity.label_ == "ORG" and tokens.pos_ == "PROPN":
            org.append(tokens.text)
    return org


def check_for_web_entities(web_entities):
    """
    checks if any web entity is present, returns those web entitiy which are labelled as "org ner"
    :param web_entities:
    :return: org 
    """
    org = []
    if web_entities:

        entities = web_entities
        for entity in entities:
            org.extend(check_for_ner(entity))
        return org


def check_for_text(text):
    """
    checks if any text is present, returns those token of text which are labelled as "org ner"
    :param text:
    :return: org
    """

    org = []
    if text:
        lines = text.split("\n")
        for line in lines:
            org.extend(check_for_ner(line))
        return org


def tag_org(text, logo, web_entities):
    """
    check the image for "org ner" in text and web entities
    :param text:
    :param logo:
    :param web_entities:
    :return: list
    """

    final_list = []

    if not logo:
        org = check_for_web_entities(web_entities)
        final_list.extend(org)
        org = check_for_text(text)
        if org:
            final_list.extend(org)

    else:
        final_list = logo

    return final_list


def detect_logo(url='', file_path=''):
    """
    takes images as an input and return list of logos present in that image
    :param: url,path
    :return: list
    """

    text, logo, web_entities = get_info(url, file_path)
    return list(set(tag_org(text, logo, web_entities)))


if __name__ == "__main__":
    print detect_logo(url="https://epaper.timesgroup.com/Olive/ODN/TimesOfIndia/get/image.ashx?kind=block&href=TOIM%2F2018%2F06%2F28&id=Ad0010027&ext=.png")
    # print detect_logo(file_path="/home/apeksha/Desktop/image.ashx.png")