#!/usr/bin/python
"""
Module to scrap Ad images from TOI newspaper
"""
import multiprocessing
import os
import time
from datetime import timedelta, datetime

import requests
import logging

from pandas import DataFrame
from multiprocessing.dummy import Pool
from tqdm import tqdm

from settings import RESULTS_DIR

__author__ = "Satish Jasthi"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class AdScraper:

    def __init__(self,
                 base_url="https://epaper.timesgroup.com/Olive/ODN/TimesOfIndia/get/prxml.ashx?",
                 payload=None
                 ):

        if payload is None:
            payload = {"kind": "page",
                       "href": None,
                       "page": 1}

        self.base_url = base_url
        # base url for files in achieve
        self.archieve_url = "https://epaper.timesgroup.com/olive/apa/timesofindia/get/prxml.ashx?"
        self.payload = payload

        self.image_url = "https://epaper.timesgroup.com/Olive/ODN/TimesOfIndia/get/TOIM-2018-06-24/image.ashx?"
        self.image_url_payload = {"kind": "block",
                                  "href": None,
                                  "id": None}
        self.results = {'Ad_ID': [], 'Page_number': [], 'bbox': [], 'Image_link': []}

    def scrap_ads_pp(self, region, date, page_number):
        """
        Method to scrap ads from newspaper of a given region, date and page_number
        :param region: (str) like M for mumbai
        :param date: (str) like "2018/06/25"
        :param page_number: (int)
        :return:
        """
        # logger.info('Calling API')
        self.payload['href'] = "TOI{}/{}".format(region, date)
        self.payload['page'] = page_number

        r = requests.get(self.base_url, params=self.payload)
        try:
            if r.status_code == 500:
                r = requests.get(self.archieve_url, params=self.payload)
                json_data = r.json()
            else:
                json_data = r.json()
            return json_data
        except Exception as e:
            print('Error: {}'.format(e))
            print('date: {}, page_num: {}'.format(date, page_number))
            json_data = None
            return json_data

    @staticmethod
    def extract_ad_data(region, date, ad_json, page_number):
        """
        Method to extract ad_id from json_file
        :param region: (str)
        :param date: (str)
        :param ad_json: (dict)
        :param page_number: (int)
        :return: (dict)
        """
        ad_list = []

        for i in range(len(ad_json['en'])):
            ad_dictionaries = ad_json['en'][i]['pr']
            for ad in ad_dictionaries:
                image_url_payload = {'kind': 'block'}
                if 'el' in ad and ad['el'] == "AdFrame":
                    ad.setdefault('page_number', page_number)
                    image_url_payload.setdefault('href', 'TOI{}/{}'.format(region, date))
                    image_url_payload.setdefault('id', ad['id'])
                    ad.setdefault('image_payload', image_url_payload)
                    ad_list.append(ad)
        return ad_list

    def get_img_url(self, image_dict):
        """
        Function to generate Ad image_url from dict
        :param image_dict: (dict)
        :return: (str)
        """
        url = "{}kind={}&href={}&id={}".format(self.image_url, image_dict['kind'], image_dict['href'], image_dict['id'])
        return url

    def scrap_ads(self, region, date, num_pages):
        """
        Method to scrap entire newspaper for Ads for given region and date
        :param region: (str) like M- Mumbai
        :param date: (str)
        :param num_pages: (int)
        :return:
        """
        def prep_dataframe(ad_list):
            """
            Function to ad related data as a df
            :param ad_list: (list) includes list of ad_dicts
            :return:
            """
            for dataframe_dict in ad_list:
                self.results.setdefault('Ad_ID', []).append(dataframe_dict['id'])
                self.results.setdefault('Page_number', []).append(dataframe_dict['page_number'])
                self.results.setdefault('bbox', []).append(dataframe_dict['box'])
                self.results.setdefault('Image_link', []).append(self.get_img_url(dataframe_dict['image_payload']))
                self.results.setdefault('Date', []).append(date)

        for page in range(1, num_pages):
            json_dict = self.scrap_ads_pp(region, date, page)
            time.sleep(0.75)
            if json_dict is not None:
                ad_data = self  .extract_ad_data(region, date, json_dict, page)
                prep_dataframe(ad_data)

    def scrap_ads_range(self, region, start_date, end_date, num_pages):
        """
            Method to scrap entire newspaper for Ads for given region and month
            :param start_date: (str), as YearMonthDate
            :param end_date: (str), as YearMonthDate
            :param region: (str) like M- Mumbai
            :param num_pages: (int)
            :return:
            """
        end_date = datetime.strptime(end_date, "%Y%m%d").date()
        start_date = datetime.strptime(start_date, "%Y%m%d").date()

        # Sequential
        # for n in tqdm(range(int((end_date - start_date).days))):
        #     date = start_date + timedelta(n)
        #     date = date.strftime("%Y/%m/%d")
        #     self.scrap_ads(region, date, num_pages)

        # Multiprocess
        dates = []
        for n in range(int((end_date - start_date).days)):
            date = start_date + timedelta(n)
            dates.append(date.strftime("%Y/%m/%d"))

        pool = Pool(multiprocessing.cpu_count())
        result_ops = [pool.apply_async(func=self.scrap_ads, args=(region, date, num_pages)) for date in dates]
        [p.get() for p in result_ops]
        pool.close()

        logger.info("Saving results as Results.csv")
        df = DataFrame.from_dict(self.results)
        df.to_csv(os.path.join(RESULTS_DIR, 'Results.csv'))

if __name__ == "__main__":
    o = AdScraper()
    o.scrap_ads_range(region='M', start_date='20171101', end_date='20180627', num_pages=7)
