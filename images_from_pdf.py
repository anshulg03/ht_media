#!/usr/bin/python
"""
Module to get images from pdfs
"""
import os
import glob
import multiprocessing
import logging
from multiprocessing.dummy import Pool

import pdfplumber
import time

from settings import PDFs_PATH, SAVE_PDF_IMG_PATH

__author__ = "Satish Jasthi"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def convert_pdf_page_to_image(pdf_page, pdf_name, page_num):
    """
    Function to convert single pdf page into image
    :param pdf_page: (pdf_plumber pdf_object)
    :param pdf_name: (str) full pdf file name
    :param page_num: (int)

    :return:
    """
    img = pdf_page.to_image(resolution=150)
    name = '{}_{}.jpg'.format(pdf_name, page_num)
    img.save(os.path.join(SAVE_PDF_IMG_PATH, name))


def convert_pdf_to_image(pdf):
    """
    Function to convert one entire pdf to images
    :param pdf: path to pdf
    :return:
    """
    pdf_name = pdf.split('/')[-1]
    with pdfplumber.open(pdf) as pdf_handle:
        pages = pdf_handle.pages[:]
        p = Pool(processes=multiprocessing.cpu_count())
        [p.apply_async(convert_pdf_page_to_image, args=(page, pdf_name, index)) for index,page in enumerate(pages)]
        p.close()


def save_images(image_tuple):
    """
    Function to save pdf_plumber image object to jpg files
    :param image: (tuple) like (image_obj, pdf_name, pg_num)
    :return:
    """
    name = '{}_{}.jpg'.format(image_tuple[1], image_tuple[2])
    image_tuple[0].save(os.path.join(SAVE_PDF_IMG_PATH, name))


def main():
    pdfs = glob.glob(PDFs_PATH + '/*.pdf')
    for pdf in pdfs:
        logger.info('Getting PDF pages')
        convert_pdf_to_image(pdf)


if __name__ == "__main__":
    start = time.time()
    main()
    print "Total_time: {} seconds".format(time.time() - start)
    # Total_time: 85.2890892029 seconds