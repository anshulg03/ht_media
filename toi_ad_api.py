import requests
import pandas as pd

url = "https://epaper.timesgroup.com/Olive/ODN/TimesOfIndia/get/prxml.ashx"


def call_toi_api(date, page):
    payload = {'kind': "page",
               'href': "TOIM/{}".format(date),
               'page': page}

    r = requests.get(url, params=payload)

    return r.json()['en']


def calc_ad_area(coordinates):
    page_width = 1650
    page_length = 2567
    width, height = coordinates[2], coordinates[3]
    return (width * height * 100) / (page_length * page_width)


def get_qtr(coordinates):
    x_min = coordinates[0]
    y_min = coordinates[1]
    width = coordinates[2]
    height = coordinates[3]

    x_max = x_min + width
    y_max = y_min + height

    qtr1, qtr2, qtr3, qtr4 = False, False, False, False

    if x_min < 875 and y_min < 1285:
        qtr1 = True
    if x_max > 875 and y_min < 1285:
        qtr2 = True
    if x_min < 875 and y_max > 1285:
        qtr3 = True
    if x_max > 875 and y_max > 1285:
        qtr4 = True
    return qtr1, qtr2, qtr3, qtr4


def create_df(date):
    df = pd.DataFrame(columns=['Date', 'Page', 'AD_ID', 'Dimension', 'Percent_Area', 'Qtr1', 'Qtr2', 'Qtr3', 'Qtr4'])
    for page in range(1, 6):
        for i in call_toi_api(date, page):
            data = {}
            if i['id'][:2] == 'Ad':
                data['Date'] = date
                data['AD_ID'] = i['pr'][-1]['id']
                data['Page'] = page
                data['Dimension'] = "{}*{}".format(i['box'][2], i['box'][3])
                data['Percent_Area'] = calc_ad_area(i['box'])
                data['Qtr1'], data['Qtr2'], data['Qtr3'], data['Qtr4'] = get_qtr(i['box'])

                df = pd.concat([df, pd.DataFrame(data, index=[0])])
    return df.reset_index(drop=True)


if __name__ == '__main__':
    df = pd.concat([create_df("2018/06/25"),
                    create_df("2018/06/24"),
                    create_df("2018/06/23"),
                    create_df("2018/06/22")])
    df.to_csv('TOIM.csv', index=False)
# 1650 2567
