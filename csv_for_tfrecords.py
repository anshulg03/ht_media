#!/usr/bin/python
"""
Module to create a csv file using image
bounding box co-ordinates from a csv file
instead of a xml file
"""
import ast
import glob
import os
from PIL import Image

from pandas import read_csv, DataFrame

from settings import RESULTS_DIR, NEWS_IMAGES, TEST_IMGS, TRAIN_IMGS, TRAIN_CSV, TEST_CSV

__author__ = "Satish Jasthi"


def get_csv(dataframe, save_dir):
    """
    Function to generate csv file with data required to create
    TF record for Object detection
    :param dataframe: (pandas dataframe)
    :param name: (str)
    :param save_dir: (str)
    :return: (str)
    """
    result_dict = {'filename': [],
                   'width': [],
                   'height': [],
                   'class': [],
                   'xmin': [],
                   'ymin': [],
                   'xmax': [],
                   'ymax': []}
    missing_files = 0
    for _, row in dataframe.iterrows():
        image_name = '{}_{}.jpg'.format(row['Date'].replace('/', '_'), row['Page_number'])
        try:
            w, h = Image.open(os.path.join(NEWS_IMAGES, image_name)).size
            bbox = ast.literal_eval(row['bbox'])
            assert type(bbox) == list
            result_dict.setdefault('filename', []).append(image_name)
            result_dict.setdefault('width',[]).append(w)
            result_dict.setdefault('height', []).append(h)
            result_dict.setdefault('class', []).append('Ads')
            result_dict.setdefault('xmin', []).append(bbox[0])
            result_dict.setdefault('ymin', []).append(bbox[1])
            result_dict.setdefault('xmax', []).append(bbox[2] + bbox[0])
            result_dict.setdefault('ymax', []).append(bbox[3] + bbox[1])
        except Exception as e:
            print("Error: bbox information not found for {}\n{}".format(image_name, e))
            missing_files += 1

    print("Number of missing files: {}".format(missing_files))
    dataframe = DataFrame.from_dict(result_dict)
    dataframe.to_csv(save_dir)

def get_relavant_df(dataframe, img_dir):
    """
    Function to get dataframe with training or test data details
    :param dataframe: (pandas df)
    :param img_dir: (str)
    :return:
    """
    imgs = glob.glob(img_dir+'/*')
    img_names = [img.split('/')[-1] for img in imgs]
    dataframe['file_name'] = ['{}_{}'.format(row['Date'].replace('/','_'), str(row['Page_number'])+'.jpg') for _, row in dataframe.iterrows()]
    new_dataframe = dataframe[dataframe['file_name'].isin(img_names)]
    return new_dataframe


def main():
    results_df = read_csv(os.path.join(RESULTS_DIR, 'Results.csv'))
    train_dataframe, test_dataframe  =  get_relavant_df(results_df, TRAIN_IMGS),  get_relavant_df(results_df, TEST_IMGS)
    get_csv(train_dataframe, TRAIN_CSV)
    get_csv(test_dataframe, TEST_CSV)

if __name__ == "__main__":
    main()