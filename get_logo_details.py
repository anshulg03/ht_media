import pandas as pd

from Logo_Detection.detect_logo import detect_logo

df = pd.read_csv('TOIM.csv')

new_df = pd.DataFrame()
for i, row in df.iterrows():
    date = row["Date"]
    ad_id = row["AD_ID"]
    url = "https://epaper.timesgroup.com/Olive/ODN/TimesOfIndia/get/image.ashx?kind=block&href=TOIM/{}&id={}&ext=.png".format(
        date, ad_id)
    row["Brand"] = detect_logo(url)

    new_df = new_df.append(row)


new_df.to_csv('TOIMumbai.csv', index=False)