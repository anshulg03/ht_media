#!/usr/bin/python
"""
Module to scrap Images of News papers from TOI
"""
import multiprocessing
import os
import urllib2
from multiprocessing.dummy import Pool
from datetime import timedelta, datetime

import urllib
import logging
from string import Template

from pandas import DataFrame
from tqdm import tqdm

from settings import  NEWS_IMAGES

__author__ = "Satish Jasthi"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class NewPaperScraper:

    def __init__(self):
        self.base_url = Template("https://epaper.timesgroup.com/olive/apa/"
                                 "timesofindia/get/image.ashx?kind=$kind&href=$href&page=$page")
        self.payload = {"kind": "page",
                       "href": None,
                       "page": 1}

    def get_news_paper(self, region, date, pages):
        """
        Method to get pages from a single news paper
        :param region: (str) like M-mumbai
        :param date: (str) like 2018/05/06
        :param pages: (int)
        :return:
        """
        for i in range(1, pages+1):
            self.payload['href'] = "TOI{}/{}".format(region, date)
            img_url = self.base_url.substitute(kind=self.payload['kind'],
                                               href=self.payload['href'],
                                               page=i)
            img_name = "{}_{}.png".format(date.replace('/', '_'), i)

            with open(os.path.join(NEWS_IMAGES,img_name), 'w') as handle:
                handle.write(urllib.urlopen(url=img_url).read())



    def scrape_news_papers(self, region, start_date, end_date, pages):
        """
            Method to get images of News papers with given number of pages
            from given region and date range
            :param region: (str) like M for Mumbai
            :param start_date: (str) like 2018/05/06
            :param end_date: (str)
            :param pages: (int)
            :return:
            """
        end_date = datetime.strptime(end_date, "%Y%m%d").date()
        start_date = datetime.strptime(start_date, "%Y%m%d").date()

        dates = []
        for n in range(int((end_date - start_date).days)):
            date = start_date + timedelta(n)
            dates.append(date.strftime("%Y/%m/%d"))

        pool = Pool(processes=multiprocessing.cpu_count())
        result_ops = [pool.apply_async(func=self.get_news_paper, args=(region, date, pages)) for date in dates]
        [p.get() for p in result_ops]
        pool.close()


if __name__ == "__main__":
    o = NewPaperScraper()
    o.scrape_news_papers('M',start_date='20171101',end_date='20180627',pages=6)
