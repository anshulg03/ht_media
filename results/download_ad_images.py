#!/usr/bin/python
"""
Module to download Ad images using multi threading
"""
import multiprocessing
import threading
import urllib
import os

from pandas import read_csv

from settings import RESULTS_DIR, AD_IMAGE_DIR

__author__ = "Satish Jasthi"

results_df = read_csv(os.path.join(RESULTS_DIR, 'Results.csv'))


class DownloadAdImages(threading.Thread):

    def __init__(self, row=None):
        threading.Thread.__init__(self)
        self.threading_lock = threading.Lock()
        self.row = row


    def get_ad_images(self, ad_id, date, url):
        """
        Function to download images using url
        :param ad_id: (str)
        :param date: (str)
        :param url: (str)
        :return:
        """
        self.threading_lock.acquire()
        file_name = os.path.join(AD_IMAGE_DIR,
                                 '{}_{}.png'.format(ad_id, date.replace('/', '_')))
        r = urllib.urlretrieve(url, file_name)
        self.threading_lock.release()


def get_df_chunks():
    """
    Function to get pandas df chunks to run multiprocess on df
    :return:
    """
    # create as many processes as there are CPUs on your machine
    num_processes = multiprocessing.cpu_count()

    # calculate the chunk size as an integer
    chunk_size = int(results_df.shape[0] / num_processes)

    # will work even if the length of the dataframe is not evenly divisible by num_processes
    chunks = [results_df.ix[results_df.index[i:i + chunk_size]] for i in range(0, results_df.shape[0], chunk_size)]

    return chunk_size


def get_ad_images_mp(df):
    """
    Function to download images using url using multiprocessing
    :param df: (dataframe)
    """
    df = df.read()  # pandas.io.parsers.TextFileReader into dataframe
    for index, row in df.iterrows():
        ad_id, date, url = row['Ad_ID'], row['Date'], row['Image_link']
        file_name = os.path.join(AD_IMAGE_DIR,
                                 '{}_{}.png'.format(ad_id, date.replace('/', '_')))
        urllib.urlretrieve(url, file_name)

def main_mp():
    df_chunks = read_csv(os.path.join(RESULTS_DIR, 'Results.csv'), chunksize=get_df_chunks(), iterator=True)
    p = multiprocessing.Process(target=get_ad_images_mp, args=(df_chunks,))
    p.start()
    p.join()


def main():
    threads = []
    for index, row in results_df.iterrows():
        each_thread = DownloadAdImages(row)
        each_thread.get_ad_images(ad_id=row['Ad_ID'], date=row['Date'], url=row['Image_link'])
        threads.append(each_thread.start())
    for each_thread in threads:
        each_thread.join()

if __name__ == "__main__":
    # main()
    main_mp()